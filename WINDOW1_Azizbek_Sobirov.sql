WITH RankedCustomers AS (
 -- Rank customers based on total sales for each channel and year
    SELECT
        c.cust_id,
        c.cust_first_name,
        c.cust_last_name,
        s.channel_id,
        EXTRACT(YEAR FROM s.time_id) AS sales_year,
        SUM(s.amount_sold) AS total_sales,
        DENSE_RANK() OVER(ORDER BY SUM(s.amount_sold) DESC) AS overall_rank
    FROM
        customers c
    JOIN
        sales s ON c.cust_id = s.cust_id
    WHERE
        EXTRACT(YEAR FROM s.time_id) IN (1998, 1999, 2001)
    GROUP BY
        c.cust_id, c.cust_first_name, c.cust_last_name, s.channel_id, EXTRACT(YEAR FROM s.time_id)
)
-- Select the top 300 customers based on overall rank
SELECT
    rc.cust_id,
    rc.cust_first_name,
    rc.cust_last_name,
    rc.channel_id AS sales_channel,
    rc.sales_year,
    rc.total_sales
FROM
    RankedCustomers rc
WHERE
    rc.overall_rank <= 300;